Feature: Search Users HRM Application

  Background:
    Given User goes on HRMLogin page "https://opensource-demo.orangehrmlive.com/" and login using "Admin" and "admin123"

  @ValidUsername
  Scenario Outline: Users enter valid username

    When User types username from "<SheetName>" and "<RowNum>"
    Then Username is exist

    Examples:
      | SheetName   | RowNum          |
      | Sheet1      | 0               |

  @InvalidUsername
  Scenario Outline: Users enter invalid username

    When User types username from "<SheetName>" and "<RowNum>"
    Then User should be able to see following prompt message

    Examples:
      | SheetName   | RowNum          |
      | Sheet1      | 1               |