package com.emeff23.pw.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(tags = "", features = {"src/test/resources/features/"}, glue = {"com.emeff23.pw.definitions"},
        plugin = {})

public class RunCucumberTests extends AbstractTestNGCucumberTests {
}
