package com.emeff23.pw.pomartifacts.common;

import com.emeff23.pw.common.CentralVars;
import com.emeff23.pw.common.PropFileMgmt;
import com.google.gson.JsonObject;
import com.microsoft.playwright.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class PwHelper {

    private static final Logger LogThis = LogManager.getLogger(PwHelper.class.getName());

    public static Playwright playwright;

    public static Browser browser;

    public static BrowserContext context;

    public static Page page;

    public static ThreadLocal<Page> threadLocalDriver = new ThreadLocal<>(); //For thread-safe execution since Java is usually not thread-safe
    public static ThreadLocal<BrowserContext> threadLocalContext = new ThreadLocal<>();

    public static void setUpDriver() {

        try {

            playwright = Playwright.create();

            if (PropFileMgmt.getPropertyValue(CentralVars.PropNameLocation).equalsIgnoreCase("lambdatest")) {

                JsonObject capabilities = new JsonObject();
                JsonObject ltOptions = new JsonObject();

                capabilities.addProperty("browsername", PropFileMgmt.getPropertyValue(CentralVars.PropNameBrowser));
                // Browsers allowed: `Chrome`, `MicrosoftEdge`, `pw-chromium`, `pw-firefox` and `pw-webkit`
                capabilities.addProperty("browserVersion", "latest");
                ltOptions.addProperty("platform", "Windows 10");
                ltOptions.addProperty("name", "Playwright Test");
                ltOptions.addProperty("build", "Playwright Java Build");
                ltOptions.addProperty("user", PropFileMgmt.getPropertyValue(CentralVars.PropNameLtUsername));
                ltOptions.addProperty("accessKey", PropFileMgmt.getPropertyValue(CentralVars.PropNameLtAccesskey));
                capabilities.add("LT:Options", ltOptions);

                String caps = URLEncoder.encode(capabilities.toString(), StandardCharsets.UTF_8);
                String cdpUrl = "wss://cdp.lambdatest.com/playwright?capabilities=" + capabilities;

                switch (PropFileMgmt.getPropertyValue(CentralVars.PropNameBrowser)) {
                    case "pw-chromium":
                        browser = playwright.chromium().connect(cdpUrl);
                    default:
                }
            } else {
                switch (PropFileMgmt.getPropertyValue(CentralVars.PropNameBrowser)) {
                    case "chromium":

                        List<String> args = new ArrayList<>();
                        args.add("--window-size=1920,1080");
                        args.add("--start-maximized");
                        args.add("--remote-allow-origins=*");
                        args.add("--allow-insecure-localhost");

                        if (PropFileMgmt.getPropertyValue(CentralVars.PropNameBrowserMode).equalsIgnoreCase("headless")) {
                            browser = playwright.chromium()
                                    .launch(new BrowserType.LaunchOptions().setArgs(args));
                        } else {
                            browser = playwright.chromium()
                                    .launch(new BrowserType.LaunchOptions().setArgs(args).setHeadless(false));
                        }
                    default:
                }
            }

            context = browser.newContext();
            page = context.newPage();

            threadLocalDriver.set(page);
            threadLocalContext.set(context);

        } catch (Exception e) {
            LogThis.error("Exception e msg = " + e.getMessage());
            LogThis.error("Exception e  = " + e);
        }

    }

    public static void openPage(String url) {

        page.navigate(url);

    }

    public static void pwTearDown() {

        page.close();
        context.close();
        browser.close();

    }

    public static synchronized Page getPage() {

        return threadLocalDriver.get(); // Will return Initialized Thread Local Driver

    }

    public static synchronized BrowserContext getContext() {

        return threadLocalContext.get(); // Will return Initialized Thread Local Context

    }

}
