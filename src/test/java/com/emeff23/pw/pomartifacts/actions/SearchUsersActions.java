package com.emeff23.pw.pomartifacts.actions;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class SearchUsersActions {

    private final Locator searchUsernameField;

    private final Locator searchBtn;

    private final Locator searchErrorMessage;

    public SearchUsersActions(Page page) {

        this.searchUsernameField = page
                .locator("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[1]/div[2]/form/div[1]/div/div[1]/div/div[2]/input");
        this.searchBtn = page
                .locator("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[1]/div[2]/form/div[2]/button[2]");
        this.searchErrorMessage = page
                .locator("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/span");

    }

    public void search(String searchUsername) {

        searchUsernameField.fill(searchUsername);

        searchBtn.click();

    }

    public Locator getSearchErrorMessage() {

        return searchErrorMessage;

    }

}
