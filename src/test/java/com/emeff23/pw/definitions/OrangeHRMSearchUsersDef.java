package com.emeff23.pw.definitions;

import com.emeff23.pw.common.CucumberExcelMgmt;
import com.emeff23.pw.pomartifacts.actions.LoginActions;
import com.emeff23.pw.pomartifacts.actions.MenuActions;
import com.emeff23.pw.pomartifacts.actions.SearchUsersActions;
import com.emeff23.pw.pomartifacts.common.PwHelper;
import com.microsoft.playwright.Locator;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.time.Duration;
import java.util.List;
import java.util.Map;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class OrangeHRMSearchUsersDef {

    private static final Logger LogThis = LogManager.getLogger(OrangeHRMSearchUsersDef.class.getName());

    LoginActions objLogin = new LoginActions(PwHelper.getPage());

    MenuActions menuActions = new MenuActions(PwHelper.getPage());

    SearchUsersActions searchUsersActions = new SearchUsersActions(PwHelper.getPage());

    String userName, searchErrorMessage;

    @Given("User goes on HRMLogin page {string} and login using {string} and {string}")
    public void loginTest(String url, String userName, String passWord) {

        PwHelper.openPage(url);

        objLogin.login(userName, passWord);

        menuActions.navigateMenuAdmin();

    }

    @When("User types username from {string} and {string}")
    public void searchUsername(String sheetName, String rowNum) {

        try {

            CucumberExcelMgmt excelMgmt = new CucumberExcelMgmt();

            List<Map<String, String>> excelTestData = excelMgmt
                    .getData("excel/td/TD-C-OrangeHRMSearchUsers.xlsx", sheetName);

            int rowNumInt = Integer.parseInt(rowNum);

            userName = excelTestData.get(rowNumInt).get("Username");

            searchErrorMessage = excelTestData.get(rowNumInt).get("StatusMessage");

            searchUsersActions.search(userName);

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

            Assert.fail("Exception e = " + e.getMessage());

        }

    }

    @Then("Username is exist")
    public void searchSuccessful() {

        try {

            Locator usernameInList = PwHelper.getPage().locator("(//div[contains(text(),'" + userName + "')])[1]");

            assertThat(usernameInList).hasText(userName);

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

            Assert.fail("Exception e = " + e.getMessage());

        }

    }

    @Then("User should be able to see following prompt message")
    public void searchFailed() {

        try {

            Thread.sleep(Duration.ofSeconds(3).toMillis());

            // Verify error message
            assertThat(searchUsersActions.getSearchErrorMessage()).hasText(searchErrorMessage);

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

            Assert.fail("Exception e = " + e.getMessage());

        }

    }

}
