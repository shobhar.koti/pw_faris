package com.emeff23.pw.definitions;

import com.emeff23.pw.pomartifacts.actions.DashboardActions;
import com.emeff23.pw.pomartifacts.actions.LoginActions;
import com.emeff23.pw.pomartifacts.common.PwHelper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.time.Duration;

import static com.microsoft.playwright.assertions.PlaywrightAssertions.assertThat;

public class OrangeHRMLoginDef {

    private static final Logger LogThis = LogManager.getLogger(OrangeHRMLoginDef.class.getName());

    LoginActions loginActions = new LoginActions(PwHelper.getPage());

    DashboardActions dashboardActions = new DashboardActions(PwHelper.getPage());

    @Given("User goes on HRMLogin page {string}")
    public void loginTest(String url) {

        PwHelper.openPage(url);

    }

    @When("User types username as {string} and password as {string}")
    public void goToHomePage(String userName, String passWord) {

        // login to application
        loginActions.login(userName, passWord);

        // go the next page

    }

    @Then("User should be able to login successfully and navigate to new page")
    public void verifyLogin() {

        try {

            Thread.sleep(Duration.ofSeconds(1).toMillis());

            // Verify home page
            assertThat(dashboardActions.getHomepageTextLocator()).hasText("Time at Work");

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

            Assert.fail("Exception e = " + e.getMessage());

        }

    }

    @Then("User should be able to see following error message {string}")
    public void verifyErrorMessage(String expectedErrMsgInvalidUsername) {

        try {

            Thread.sleep(Duration.ofSeconds(3).toMillis());

            // Verify error message
            //Assert.assertEquals(loginActions.getErrMsgInvalid().textContent(), expectedErrMsgInvalidUsername);
            assertThat(loginActions.getErrMsgInvalid()).hasText(expectedErrMsgInvalidUsername);

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

            Assert.fail("Exception e = " + e.getMessage());

        }

    }

    @Then("User should be able to see a message {string} below Username")
    public void verifyMissingUsernameMessage(String message) {

        try {

            Thread.sleep(Duration.ofSeconds(1).toMillis());

            //Assert.assertEquals(loginActions.getErrMsgMissingUsername().textContent(), message);
            assertThat(loginActions.getErrMsgMissingUsername()).hasText(message);

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

            Assert.fail("Exception e = " + e.getMessage());

        }

    }

}
