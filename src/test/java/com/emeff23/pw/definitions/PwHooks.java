package com.emeff23.pw.definitions;

import com.emeff23.pw.common.CentralVars;
import com.emeff23.pw.common.FileMgmt;
import com.emeff23.pw.common.JiraMgmt;
import com.emeff23.pw.common.PropFileMgmt;
import com.emeff23.pw.pomartifacts.common.PwHelper;
import io.cucumber.java.*;
import io.cucumber.java.Scenario;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PwHooks {

    private static final Logger LogThis = LogManager.getLogger(PwHooks.class.getName());

    @Before
    public void setUp() {

        try {

            PwHelper.setUpDriver();

        } catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

        }

    }

    @After
    public void tearDown(Scenario scenario) {

        try {

            LogThis.debug("Scenario : " + scenario.getName() + " (" + scenario.getId() + ")");

            String testResult = scenario.getStatus().toString();

            LogThis.debug(testResult);

            //validate if scenario has failed
            if (scenario.isFailed()) {

                List<String> screenshotPathList = new ArrayList<>();

                Map<byte[], String> screenshotObj = FileMgmt.saveScreenshot(scenario.getName());

                for (Map.Entry<byte[], String> entry : screenshotObj.entrySet()) {

                    scenario.attach(entry.getKey(), "image/png", scenario.getName());

                    screenshotPathList.add(entry.getValue());

                }

                String getPropJiraEnabled = PropFileMgmt.getPropertyValue(CentralVars.PropNameJiraEnabled);

                LogThis.debug("Is Jira enabled = " + getPropJiraEnabled);

                if (getPropJiraEnabled.equalsIgnoreCase("true")) {

                    cucumberLogToJira(scenario, screenshotPathList);

                }

            }

            PwHelper.pwTearDown();

        }  catch (Exception e) {

            LogThis.error("Exception e = " + e.getMessage());

        }

    }

    private static void cucumberLogToJira(Scenario scenario, List<String> attachmentPathList) {

//Provide proper Jira project URL (Do not forget the ending slash) ex: https://browsertack.atlassian.net/
//Jira User name ex: browserstack@gmail.com
//API token copy from Jira dashboard ex: lorelimpusm12uijk
//Project key should be, Short name ex: BS

        JiraMgmt JiraServiceProvider = new JiraMgmt(PropFileMgmt.getPropertyValue(CentralVars.PropNameJiraUrl),
                PropFileMgmt.getPropertyValue(CentralVars.PropNameJiraUsername),
                PropFileMgmt.getPropertyValue(CentralVars.PropNameJiraToken),
                PropFileMgmt.getPropertyValue(CentralVars.PropNameJiraProject));

        String issueDescription = "Failure Reason from Automation Testing\n\n" + scenario.getName()
                + " condition does not met. "
                + "\n";

        String issueSummary = scenario.getName()
                + " Failed in Automation Testing";

        LogThis.debug("issueSummary = " + issueSummary);

        LogThis.debug("issueDescription = " + issueDescription);

        JiraServiceProvider.createJiraIssue("Bug",
                UUID.randomUUID() + "-Cucumber-" + issueSummary,
                issueDescription, attachmentPathList);

    }

}
